﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ManagerBase : MonoBehaviour
{
    // needs to be able to select a unit
    // needs to be able to tell a unit to move
    // needs to be able to deselect units
    // needs to be able to open a menu for spells / abilities
    // needs to be able to open a menu to spawn units

    // needs to be able to end turn

    public MapUnit selectedUnit = null;

    public virtual void SelectUnit(MapUnit unit)
    {
        if(unit)
        {
            if (unit.moves > 0)
            {
                selectedUnit = unit;
                selectedUnit.SelectMe(true);
            }
            else
            {
                // show error message "unit cannot move any more this turn"
            }
        }
    }
    public virtual void DeSelectUnit()
    {
        selectedUnit.SelectMe(false);
        selectedUnit = null;
    }


    public virtual void MoveUnit(MapTile tile)
    {
        if (tile && selectedUnit)
        {
            if (selectedUnit.moves > 0)
                tile.Selected(selectedUnit);

            else
                DeSelectUnit();
        }
    }


    public virtual void EndTurn()
    {
        DeSelectUnit();
        // end turn
    }
}
