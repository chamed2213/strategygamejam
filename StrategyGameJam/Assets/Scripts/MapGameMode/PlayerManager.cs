﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : ManagerBase
{
    private void Update()
    {
        RaycastPointer();
        Deselection();
    }

    private void Deselection()
    {
        if(selectedUnit)
        {
            if (Input.GetMouseButtonDown(1))
                DeSelectUnit();
        }
    }

    private void RaycastPointer()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            BaseSelection(hit);
            TileSelection(hit);
        }

    }

    void BaseSelection(RaycastHit hit)
    {
        if (!selectedUnit)
        {
            MapUnit unit = hit.transform.GetComponent<MapUnit>();
            if (unit)
            {
                if (Input.GetMouseButtonDown(0))
                    SelectUnit(unit);
            }
        }
    }

    void TileSelection(RaycastHit hit)
    {
        if(selectedUnit)
        {
            MapUnit unit = hit.transform.GetComponent<MapUnit>();
            MapTile tile = unit ? unit.GetCurrentTile() : hit.transform.GetComponent<MapTile>();

            if(tile)
            {
                if (Input.GetMouseButtonDown(0))
                    MoveUnit(tile);
            }
        }
    }
}
