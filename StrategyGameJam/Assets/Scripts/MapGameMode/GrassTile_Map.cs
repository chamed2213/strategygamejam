﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassTile_Map : MapTile
{
    public override int CheckCost(MapUnit unit)
    {
        // If unit can walk mountaint
        // return 1
        // else 
        // return 2

        return 1;
    }

    public override void Entered(MapUnit unit)
    {
        Debug.Log(unit.name + " has entered a grass tile");
    }

    public override void Exited(MapUnit unit)
    {
        Debug.Log(unit.name + " has exited a grass tile");
    }

    public override void Selected(MapUnit unit)
    {
        Debug.Log(unit.name + " is selecting a grass tile a grass tile");

        unit.Move(this);
    }
}
