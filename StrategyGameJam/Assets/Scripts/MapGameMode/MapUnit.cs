﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapUnit : MonoBehaviour
{
    public bool active = false;
    public int moves = 3;

    public void SelectMe(bool setting)
    {
        active = setting;
    }
    public void Move(MapTile tile)
    {
        // If we are currently not moving
        if(movingMap == null)
        {
            // Check if we have enough cost
            // Check if the space has a unit already
            // If niether returns true we can move
            MapTile current = GetCurrentTile();

            // pulls the cost calculation from the tile
            int cost = tile.CheckCost(this);

            // Is my "moves" greater than or equal to the cost
            bool canMove = cost <= moves;
            // Is the tile currently occupied?
            bool owned = tile.CheckOwned();
            // Is the tile adjacent to my current tile
            bool isAdjacent = current.adjacencyList.Contains(tile);

            // ARE THINGS RIGHT?
            if (canMove && !owned && isAdjacent)
            {
                // Move 
                moves -= cost;
                current.Exited(this);


                // rotation calcuation
                Vector3 lookPos = tile.MovePosition - transform.position;
                lookPos.y = transform.position.y;
                Quaternion rotation = Quaternion.LookRotation(lookPos, Vector3.up);
                LeanTween.rotate(gameObject, rotation.eulerAngles, 0.2f).setEaseOutSine();

                movingMap = tile;
                LeanTween.move(gameObject, tile.MovePosition, 0.33f).setEaseOutSine().setOnComplete(EnterTile);
            }
            else
            {
                // Show error
            }
        }
    }

    MapTile movingMap = null;
    void EnterTile()
    {
        movingMap.Entered(this);
        movingMap = null;
    }

    public virtual MapTile GetCurrentTile()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position + new Vector3(0, -0.5f), 0.5f);

        foreach (var item in colliders)
        {
            MapTile tile = item.GetComponent<MapTile>();

            if (tile)
                return tile;
        }

        return null;
    }
}
