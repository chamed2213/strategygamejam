﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public abstract class MapTile : MonoBehaviour
{
    public Transform moveTarget;
    public Vector3 MovePosition
    {
        get
        {
            if (moveTarget)
                return moveTarget.position;

            else return transform.position;
        }
    }

    public List<MapTile> adjacencyList = new List<MapTile>();

    // Start is called before the first frame update
    protected virtual void Start()
    {
        FindAdjacents();
    }

    // Will be used to check if the unit in question can enter the space or not
    public abstract int CheckCost(MapUnit unit);
    public virtual bool CheckOwned()
    {
        return GetUnitOnTop();
    }

    // Call when a unit selects a space 
    public abstract void Selected(MapUnit unit);
    // Call when a unit enters a space
    public abstract void Entered(MapUnit unit);
    // Call when a unit exits a space
    public abstract void Exited(MapUnit unit);

    #region Tile Detection

    public virtual MapUnit GetUnitOnTop()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position + new Vector3(0, 0.5f), 0.5f);

        foreach (var item in colliders)
        {
            MapUnit unit = item.GetComponent<MapUnit>();

            if (unit)
                return unit;
        }

        return null;
    }

    protected virtual void FindAdjacents()
    {
        // Gets all colliders within radius 1m
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1);

        // Checks all colliders detected
        foreach (var item in colliders)
        {
            // Finds the MapTile component detected in a collider
            MapTile tile = item.GetComponent<MapTile>();

            // If the tile does not come up as NULL (Collider has a MapTile)
            // And we are not checking ourselves (We don't want to be in our list)
            if (tile && tile != this)
            {
                // If the list DOES NOT have the tile already in it, add it to the list
                if (!adjacencyList.Contains(tile))
                    adjacencyList.Add(tile);
            }
        }
    }

    #endregion Tile Detection
}
