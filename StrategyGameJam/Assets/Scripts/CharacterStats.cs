﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterStats 
{
    public float health = 20;
    public float maxHP = 20;

    public float currentHP { get { return health / maxHP; } }
}
