﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterUnit : MonoBehaviour
{
    public string characterName = "new character";
    public CharacterClass characterClass;
    public bool frontLine = true;

    [Range(0,20)]
    public int characterLevel = 1;
    public float experience = 0;

    public Ability ability;

    [SerializeField]
    public CharacterStats myStats;

    #region Events

    public event Action<float, CharacterUnit, CharacterUnit> OnDamageRecieved_Local;
    public static event Action<float, CharacterUnit, CharacterUnit> OnDamageRecieved_Global;

    #endregion Events

    #region Delegates

    public delegate void OnDamageDealtEffect(ref float damage, ref CharacterUnit target);
    public OnDamageDealtEffect onDamageDealt;

    public delegate void OnDamageRecievedEffect(ref float damage, ref CharacterUnit attacker);
    public OnDamageRecievedEffect onDamageRecieved;

    #endregion Delegates

    private void Awake()
    {
        characterClass.UnloadClass(this);
    }


    public void TakeDamage(float value, CharacterUnit attacker)
    {
        onDamageRecieved(ref value, ref attacker);

        myStats.health -= value;

        OnDamageRecieved_Local?.Invoke(value, this, attacker);
        OnDamageRecieved_Global?.Invoke(value, this, attacker);
    }
    public void DealDamage(float damage, CharacterUnit target)
    {
        onDamageDealt(ref damage, ref target);

        target.TakeDamage(damage, this);
    }
}

public abstract class Ability
{
    public CharacterUnit owner;
    protected void SetOwner(CharacterUnit unit)
    {
        owner = unit;
    }

    public abstract void AssignToDelegate(CharacterUnit unit);
}

public class Ability_RockGuard : Ability
{
    public override void AssignToDelegate(CharacterUnit unit)
    {
        SetOwner(unit);

        unit.onDamageRecieved += OurEffect;
    }

    private void OurEffect(ref float damage, ref CharacterUnit target)
    {
        damage -= 5;

        target.TakeDamage(damage * 0.5f, null);
    }
}
public class Ability_ArrowCatcher : Ability
{
    public override void AssignToDelegate(CharacterUnit unit)
    {
        SetOwner(unit);

        throw new System.NotImplementedException();
    }
}

public class LocalSadist : Ability
{
    public override void AssignToDelegate(CharacterUnit unit)
    {
        SetOwner(unit);

        CharacterUnit.OnDamageRecieved_Global += VampireHealth;
    }

    private void VampireHealth(float damage, CharacterUnit defender, CharacterUnit attacker)
    {
        float heal = damage * 0.1f;

        if (defender != owner)
            owner.myStats.health += heal;
    }
}