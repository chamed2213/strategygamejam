﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterAbility
{
    rockGuard, arrowCatcher, helpIHaveNoIdea
}

[CreateAssetMenu(fileName = "New Class", menuName = "Tactics/New Class", order = 0)]
public class CharacterClass : ScriptableObject
{
    [Header("Main Attributes")]
    public string ClassName = "New Class";
    public int maxHealth = 10;

    [Header("Class Skills")]
    public CharacterAbility ability;
    public string attack1;

    [Header("Character Art")]
    public Sprite icon; 
    public Sprite portrait;
    public GameObject model;

    public void UnloadClass(CharacterUnit unit)
    {
        unit.myStats.health = maxHealth;
        unit.myStats.maxHP = maxHealth;

        unit.ability = SetAbility();
    }

    public Ability SetAbility()
    {
        switch(ability)
        {
            default: // rockguard
                return new Ability_RockGuard();

            case CharacterAbility.arrowCatcher:
                return new Ability_ArrowCatcher();
        }
    }
}
