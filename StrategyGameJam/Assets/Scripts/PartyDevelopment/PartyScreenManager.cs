﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyScreenManager : MonoBehaviour
{
    public Transform selectionTarget;
    public Transform[] selectionPositions;

    public PartyComposition selection;
    public CharacterUnit classChangeSelect;

    public PartyViewerManager viewer;

    public static event Action OnReturnToArmyManager_Event;
    public static event Action OnStartChangeClass_Event;
    public static event Action OnEndChangeClass_Event;

    private void Awake()
    {
        transform.position = new Vector2(Screen.width * 1.5f, Screen.height / 2);
    }

    private void Start()
    {
        ArmyScreenManager.OnPartySelection_Event += OpenScreen;
        UI_Class_ClassSelection.OnClassChange += ChangeSelectedClass;
    }
    private void OnDestroy()
    {
        ArmyScreenManager.OnPartySelection_Event -= OpenScreen;
        UI_Class_ClassSelection.OnClassChange -= ChangeSelectedClass;
    }


    private void ChangeSelectedClass(CharacterClass setClass)
    {
        if(classChangeSelect)
        {
            classChangeSelect.characterClass = setClass;
        }

        OnEndChangeClass_Event?.Invoke();
    }
    private void OpenScreen(PartyComposition party)
    {
        selection = party;

        Vector2 position = new Vector2(Screen.width / 2, Screen.height / 2);
        LeanTween.move(gameObject, position, 0.25f);

        viewer.RecieveParty(selection);

        BUTTON_SetClassChangeAndOpenMenu(0);
    }
    private void CloseScreen()
    {
        Vector2 position = new Vector2(Screen.width * 1.5f, Screen.height / 2);
        LeanTween.move(gameObject, position, 0.25f);
    }

    public void BUTTON_ReturnToArmy()
    {
        OnReturnToArmyManager_Event?.Invoke();

        CloseScreen();
    }
    public void BUTTON_SetClassChangeAndOpenMenu(int index)
    {
        classChangeSelect = selection.party[index];
        selectionTarget.position = selectionPositions[index].position;

        OnStartChangeClass_Event?.Invoke();
    }
}
