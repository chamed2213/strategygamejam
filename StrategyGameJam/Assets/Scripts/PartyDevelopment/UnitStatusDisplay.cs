﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitStatusDisplay : MonoBehaviour
{
    public CharacterUnit reference;

    [Header("UI Attributes")]
    public Slider healthBar;

    public Image icon, position;
    public Sprite front, back;

    private void Awake()
    {
        reference = GetComponent<CharacterUnit>();

        AssignDefinition();
    }
    private void Start()
    {
        PartyScreenManager.OnEndChangeClass_Event += AssignDefinition;
    }
    private void OnDestroy()
    {
        PartyScreenManager.OnEndChangeClass_Event -= AssignDefinition;
    }

    public void AssignDefinition()
    {
        if(reference)
        {
            if (icon)
                icon.sprite = reference.characterClass.icon;

            if (healthBar)
                healthBar.value = reference.myStats.currentHP;

            if (position)
                position.sprite = reference.frontLine ? front : back;
        }
    }
}
