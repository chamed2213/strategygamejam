﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyViewerManager : MonoBehaviour
{
    public UnitStatusDisplay[] displays;
    public UI_Party_TogglePosition[] positionings;

    public void RecieveParty(PartyComposition party)
    {
        for(int i = 0; i < 3; i++)
        {
            CharacterUnit unit = party.party[i];

            displays[i].reference = unit;
            displays[i].AssignDefinition();

            positionings[i].SetUp(unit);
        }
    }

    public void SaveChanges(PartyComposition party)
    {
        for (int i = 0; i < 3; i++)
        {
            party.party[i] = displays[i].reference;
        }
    }
}
