﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Party_TogglePosition : MonoBehaviour
{
    public CharacterUnit reference;

    public Image icon;
    public Sprite front, back;

    public void SetUp(CharacterUnit unit)
    {
        reference = unit;
        SetIcon();
    }

    public void BUTTON_TogglePosition()
    {
        if(reference)
        {
            reference.frontLine = !reference.frontLine;
        }

        SetIcon();
    }

    void SetIcon()
    {
        if(reference)
            icon.sprite = reference.frontLine ? front : back;
    }
}
