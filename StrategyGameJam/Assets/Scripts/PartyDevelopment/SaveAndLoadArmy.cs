﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadArmy : MonoBehaviour
{
    public static SaveAndLoadArmy instance;
    public ArmyScreenManager army;

    private void Awake()
    {
        if (instance && instance != this) Destroy(gameObject);
        else instance = this;
    }

    public static void SaveArmy()
    {
        SaveData save = new SaveData(instance.army);
    }
}

public class SaveData
{
    public string[] classSettings = new string[27];
    public bool[] positionSettings = new bool[27];

    public SaveData (ArmyScreenManager army)
    {
        for(int i = 0; i < army.parties.Length; i++)
        {
            for (int v = 0; v < 3; v++)
            {
                int index = (i * 3) + v;
                classSettings[index] = army.parties[i].party[v].characterClass.name;
                positionSettings[index] = army.parties[i].party[v].frontLine;

                Debug.Log("Saved character of i : " + i + " - with v : " + v + " with result of : " + index + " - " +
                    "class : " + classSettings[index] + " - frontline : " + positionSettings[index]);
            }
        }
    }
}