﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_Class_ClassSelection : MonoBehaviour
{
    public CharacterClass setClass;

    public Image icon;
    public TextMeshProUGUI text;

    public static event Action<CharacterClass> OnClassChange;

    private void Awake()
    {
        if (setClass)
        {
            icon.sprite = setClass.icon;
            text.text = setClass.ClassName;
        }
        else
            gameObject.SetActive(false);
    }

    public void BUTTON_PushClassChangeSelection()
    {
        OnClassChange?.Invoke(setClass);
    }
}
