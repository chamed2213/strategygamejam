﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmyScreenManager : MonoBehaviour
{
    public PartyComposition[] parties = new PartyComposition[9];

    public static event Action<PartyComposition> OnPartySelection_Event;

    private void Awake()
    {
        transform.position = new Vector2(Screen.width / 2, Screen.height / 2);
    }

    private void Start()
    {
        PartyScreenManager.OnReturnToArmyManager_Event += OpenScreen;
    }
    private void OnDestroy()
    {
        PartyScreenManager.OnReturnToArmyManager_Event -= OpenScreen;
    }

    private void OpenScreen()
    {
        Vector2 position = new Vector2(Screen.width / 2, Screen.height / 2);
        LeanTween.move(gameObject, position, 0.25f);

        SaveAndLoadArmy.SaveArmy();
    }
    private void CloseScreen()
    {
        Vector2 position = new Vector2(Screen.width * -0.5f, Screen.height / 2);
        LeanTween.move(gameObject, position, 0.25f);
    }

    public void BUTTON_GoToPartyManagement(int index)
    {
        OnPartySelection_Event?.Invoke(parties[index]);

        CloseScreen();
    }
}
