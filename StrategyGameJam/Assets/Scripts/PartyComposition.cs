﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyComposition : MonoBehaviour
{
    public CharacterUnit[] party = new CharacterUnit[3];
    UnitStatusDisplay[] displays;

    [Header("UI Attributes")]
    public PartyViewerManager viewer;

    private void Awake()
    {
        displays = new UnitStatusDisplay[party.Length];

        for(int i = 0; i < party.Length; i++)
        {
            displays[i] = party[i].GetComponent<UnitStatusDisplay>();
        }
    }

    private void Start()
    {
        PartyScreenManager.OnReturnToArmyManager_Event += RefreshDisplays;
    }
    private void OnDestroy()
    {
        PartyScreenManager.OnReturnToArmyManager_Event -= RefreshDisplays;
    }

    public void RefreshDisplays()
    {
        foreach (var item in displays)
            item.AssignDefinition();
    }
}
